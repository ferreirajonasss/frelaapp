from django.contrib import admin
from usuarios.models import Frelancer, Contratante

# Register your models here.
admin.site.register(Frelancer)
admin.site.register(Contratante)
