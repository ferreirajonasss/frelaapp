from django.db import models
from django.contrib.auth.models import User


class PerfilFrelancer(models.Model):
    descricao = models.TextField()


class Frelancer(models.Model):
    usuario = models.ForeignKey(User)
    perfil = models.ForeignKey(PerfilFrelancer)


class PerfilContratante(models.Model):
    descricao = models.TextField()


class Contratante(models.Model):
    usuario = models.ForeignKey(User)
    perfil = models.ForeignKey(PerfilContratante)
