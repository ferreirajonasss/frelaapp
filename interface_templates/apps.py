from django.apps import AppConfig


class InterfaceTemplatesConfig(AppConfig):
    name = 'interface_templates'
