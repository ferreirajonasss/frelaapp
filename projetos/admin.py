from django.contrib import admin
from projetos.models import Projeto, Categoria

# Register your models here.
admin.site.register(Projeto)
admin.site.register(Categoria)
