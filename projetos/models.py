from django.db import models
from usuarios.models import Frelancer


class Categoria(models.Model):
    nome = models.CharField(max_length=80)


class Projeto(models.Model):
    titulo = models.CharField(max_length=80)
    descricao = models.TextField()
    categoria = models.ForeignKey(Categoria)
    interessados = models.ManyToManyField(Frelancer)


class Proposta(models.Model):
    valor = models.FloatField()
    prazo = models.PositiveIntegerField(default=1)
    projeto = models.ForeignKey(Projeto)
